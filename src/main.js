import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import installElementPlus from './plugins/element'

const app=createApp(App)
app.use(router)
app.use(store)
app.use(installElementPlus)
app.mount('#app')
