import { createRouter, createWebHistory } from 'vue-router'
import Today from '../views/Today.vue'
import Weekly from '../views/Weekly.vue'
import Monthly from '../views/Monthly.vue'
const routes = [
    {
      path: '/Today',
      name: 'Today',
      component: Today,
    },
    {
      path: '/Weekly',
      name: 'Weekly',
      component: Weekly,
    },
    {
      path: '/Monthly',
      name: 'Monthly',
      component: Monthly,
    },
]

const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes,
    linkActiveClass:'active'
})

export default router