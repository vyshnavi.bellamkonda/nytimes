import { createStore } from "vuex";
import createPersistedState from 'vuex-persistedstate'
const store= createStore({
  plugins: [createPersistedState({
    storage: window.sessionStorage,
  })],
  state: {
    today:[],
    monthly:[],
    weekly:[]
  },
  mutations:{
    setToday:(state,products)=>state.today=products,
    setMonthly:(state,products)=>state.monthly=products,
    setWeekly:(state,products)=>state.weekly=products
  },
  actions:{
    async fetchData(ctx){
      const ids=[1,7,30];
      let res = await Promise.all(ids.map(id=>window.fetch(`https://api.nytimes.com/svc/mostpopular/v2/emailed/${id}.json?api-key=fZOmAk9EAzCPaQVNnxVpPWrmWv6A6jX6`)));  
      const json=await Promise.all(res.map(data=>data.json()))
      ctx.commit("setToday",json[0].results);
      ctx.commit("setWeekly",json[1].results);
      ctx.commit("setMonthly",json[2].results);
      console.log(store);
    }
  }
});

export default store;
